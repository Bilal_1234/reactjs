import React, { useState } from 'react'
import './Loginform.css'
const Loginform = () => {
    const[email,setEmail]=useState('')
    const[password,setPassword]=useState('')
     
    const  Register = async() =>{
        let item={email,password}
        console.log(item)

        let result= await fetch("https://drasql-backend.herokuapp.com/api/login",{
          method: 'POST', 
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(item),
        })
        result= await result.json()
        console.log("result",result)
    }
  return (
    <>
    <div>
      <div className='login-form'> 
      <h1>Log in Form  </h1> 
      <h3>E-mail  </h3>
      <input type='email' value={email} onChange={(e)=>setEmail(e.target.value)} placeholder='User Email' className='input'/>
      <h3>Password</h3>
      <input type={'password'} value={password} onChange={(e)=>setPassword(e.target.value)} placeholder='password' className='input'/>
      <br /><br />
      <button type='submit'className='submit' onClick={Register}>Submit</button>
      <button className='reset'>Reset</button>
      
    </div>
    </div>
    </>
  )
}

export default Loginform 
