import React, { useState } from 'react'
import './Mytodo.css';

const Mytodo = () => {
    const [input, setInput] = useState('');
    const [items, setItems] = useState([]);
    const addItem = () => {
        if (!input) {

        } else {
            let arr = items;
            arr.push(input);
            setItems(arr)
            // setItems([...items, input])
            setInput('');
            console.log("items :", items);
        }
    }
    const deleteItem = (id) => {
        const updatedItems = items.filter((elem, index) => {
            return index !== id
        });
        setItems(updatedItems);
    }
    const removeAll = () => {
        setItems([]);
    }
    return (

        <div className='main-div'>
            <div className='center-div'>
                <h1> Todo List 📝   </h1>
                <input type="text" placeholder='Enter Your Items ✏️ '
                    value={input} onChange={(e) => setInput(e.target.value)} />
                <button onClick={addItem} className='add-btn'>+</button>
                <ol>
                    {
                        items.map((elem, index) => {
                            return (
                                <div className='each-item' key={index}>
                                    <button onClick={() => deleteItem(index)} className='delete-btn'>❌ </button>
                                    <input type='checkbox' className='check-box' />
                                    <li>{elem} </li>

                                </div>
                            )
                        })
                    }
                </ol>

            </div>
            <button onClick={removeAll} className='clr-btn'>Remove All 🌪</button>
        </div>

    )
}

export default Mytodo
