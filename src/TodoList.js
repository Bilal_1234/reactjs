import React, { useState } from 'react'
import './TodoList.css'
  
function TodoList() {
  const [inputList, setInputList] = useState("");
  const [items, setItems] = useState([]);

  const itemEvent = (event) => {
    setInputList(event.target.value);
  };
  const listOfItems = () => {
    setItems((oldItems) => {
      return [...oldItems, inputList];
    })
  };
  const deleteItem = () => {
    setItems(items.filter((index) => index !== index));
  };
  return (
    <div>
      <>
        <div className='main-div'>
          <div className='center-div'>
            <h1>To Do List </h1>
            <input type="text" placeholder='Enter Your Items ' onChange={itemEvent} />
            <button onClick={listOfItems}>+</button>

            <ol>
              
              {
                
                items.map((items , index) => {
                  return (
                    <div className='map'>
                      <button className='btn' onClick={() => deleteItem(index,1)}>❌
                      </button>
                      <li key={index}>{items}</li>
                    </div>)
                    
                })

              }
            </ol>
          </div>
        </div>

      </>
    </div>
  )
}

export default TodoList
