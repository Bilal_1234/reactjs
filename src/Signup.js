import React, { useState } from 'react'
import './Signup.css'
const Signup = () => {
    const[name,setName]=useState('')
    const[email,setEmail]=useState('')
    const[companyname,setCompanyname]=useState('')
    const[password,setPassword]=useState('')
     
    const  Register = async() =>{
        let item={name,email,password,companyname}
        console.log(item)

        let result= await fetch("https://drasql-backend.herokuapp.com/api/register",{
          method: 'POST', 
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(item),
        })
        result= await result.json()
        console.log("result",result)
    }
  return (
    <>
    <div>
      <div className='login-form'> 
      <h1>Sign Up Form  </h1> 
      <h3>UserName  </h3>
      <input type='text' value={name} onChange={(e)=>setName(e.target.value)} placeholder='UserName' className='input'/>
      <h3>company Name  </h3>
      <input type='text' value={companyname} onChange={(e)=>setCompanyname(e.target.value)} placeholder='CompanyName' className='input'/>
      <h3>E-mail  </h3>
      <input type='email' value={email} onChange={(e)=>setEmail(e.target.value)} placeholder='User Email' className='input'/>
      <h3>Password</h3>
      <input type={'password'} value={password} onChange={(e)=>setPassword(e.target.value)} placeholder='password' className='input'/>
      <br /><br />
      <button type='submit'className='submit' onClick={Register}>Submit</button>
      <button className='reset'>Reset</button>
      
    </div>
    </div>
    </>
  )
}

export default Signup
