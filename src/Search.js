import React, { useState } from 'react'

function Search() {
    const [data, setData] = useState([
        {
            "id": 1,
            "name": "Saud"
        },
        {
            "id": 2,
            "name": "Amir"
        },
        {
            "id": 3,
            "name": "hamza"
        },
        {
            "id": 4,
            "name": "bilal"
        },
    ])
    const onChangeHandler = (e) => {
        if(e.target.value==""){
            window.location.reload(true)
            const newData=data
            setData(newData)
            return
        }
        const searchResult=data.filter(item=>item.name.toLowerCase().startsWith(e.target.value.toLowerCase()))
        setData(searchResult);
    }
    return (
        <div>
            <input type='text' placeholder='Search ...' onChange={onChangeHandler} />
            {
                data.map(item => {
                    return (
                        <h3>{item.name}</h3>
                    )
                })
            }
        </div>
    )
}

export default Search
